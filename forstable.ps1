# PowerShell 7 or above
Import-Module -usewindowspowershell Appx

$ConfigFile     =   "settings.json"
$PackageName    =   "Microsoft.WindowsTerminal"

# Check Package is Installed
try {
    if ([string]::IsNullOrEmpty($(Get-AppxPackage -Name $PackageName))) {
        throw 'Not Found'
    }
    else {
        $PackageLocation    =   $(Get-AppxPackage -Name $PackageName | Select-Object -ExpandProperty "InstallLocation")
        $PackageFamilyName  =   $(Get-AppxPackage -Name $PackageName | Select-Object -ExpandProperty "PackageFamilyName")
        Write-Host (@('Package Location:', ('"'+$PackageLocation +'"')))
    }
}
catch {
    Write-Host (@('Get-AppxPackage:', $_.Exception.Message))
    exit 1
}

# Check User Data Contained Directory
$UserDataLocation   =   (   ${HOME} + "\" + "AppData\Local\Packages" + "\" +
                            $PackageFamilyName.Replace("C:\Program Files\WindowsApps\", '') + "\" +
                            "LocalState"
                        )

try {
    if (Test-Path $UserDataLocation\$ConfigFile) {
        Write-Host ('Config File is Found!')
    }
    else {
        throw 'Config File is Not Found'
    }
}
catch {
    Write-Host (@('Test-Path:', $_.Exception.Message))
    exit 1
}

Write-Host (
    'UserData installed on:',
    $UserDataLocation
)

# Write Config
try {
    Get-Content $PSScriptRoot\$ConfigFile > $UserDataLocation\$ConfigFile || throw 'Writing Failed'
}
catch {
    Write-Host ($_.Exception.Message, '...')
}

Write-Host ('Finished!')